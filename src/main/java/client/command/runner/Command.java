package client.command.runner;

import lombok.Getter;

@Getter
public class Command {
    private final String command;
    private final String params;

    Command(String command, String params) {
        this.command = command.trim();
        this.params = params;
    }
}

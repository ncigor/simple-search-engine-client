package client.command.runner;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

class CommandReader {
    private  Scanner scanner;

    CommandReader(InputStream source) {
        this.scanner = new Scanner(source);
    }

    Command nextCommand() {
        printCommandPrompt();
        String commandLine = scanner.nextLine();
        String[] arguments = commandLine.split(" ", 2);
        String command = arguments[0].trim();
        String params = "";
        if (arguments.length > 1) {
            params = arguments[1].trim();
        }
        return new Command(command, params);
    }

    private void printCommandPrompt() {
        System.out.println();
        System.out.println("Command: ");
    }
}

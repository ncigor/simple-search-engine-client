package client.command.runner;

import client.command.ICommand;
import client.command.IncorrectCommandHandler;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommandManager {
    private final Map<String, ICommand> commandsMap;
    private final IncorrectCommandHandler incorrectCommandHandler;

    public CommandManager(List<ICommand> commands, IncorrectCommandHandler incorrectCommandHandler) {
        this.commandsMap = commands.stream()
                .collect(Collectors.toMap(ICommand::getCommand, Function.identity()));
        this.incorrectCommandHandler = incorrectCommandHandler;
    }

    public void run() {
        CommandReader commandReader = new CommandReader(System.in);
        while (true) {
            try {
                Command command = commandReader.nextCommand();
                ICommand commandRunner = commandsMap.get(command.getCommand());
                if(commandRunner != null) {
                    commandRunner.run(command.getParams());
                } else {
                    incorrectCommandHandler.handle();
                }
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
            }
        }
    }
}

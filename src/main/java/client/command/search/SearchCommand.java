package client.command.search;

import client.command.ICommand;
import client.web.WebClientFactory;

import java.util.Map;

public class SearchCommand implements ICommand {

    private final WebClientFactory webClientFactory;

    public SearchCommand(WebClientFactory webClientFactory) {
        this.webClientFactory = webClientFactory;
    }

    @Override
    public String getCommand() {
        return "find";
    }

    @Override
    public String helpString() {
        return "Search documents by query. Format: `find <query string>`";
    }

    @Override
    public void run(String query) {
        System.out.println("Processing...");
        String[] keys = webClientFactory.getClient("/documents")
                .setQueryVariables(Map.of("query", query.trim()))
                .get(String[].class);

        if (keys.length == 0) {
            System.out.println("Nothing found by your request");
        } else {
            System.out.println("We have found " + keys.length + " documents by your request: ");
            for (String key : keys) {
                System.out.println("\t");
                System.out.println(key);
            }
        }
    }
}

package client.command.index;

import client.command.ICommand;
import client.command.bean.Document;
import client.web.WebClientFactory;
import org.springframework.web.client.HttpClientErrorException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;

public class IndexDocumentCommand implements ICommand {

    private final WebClientFactory webClientFactory;

    public IndexDocumentCommand(WebClientFactory webClientFactory) {
        this.webClientFactory = webClientFactory;
    }

    @Override
    public String getCommand() {
        return "index";
    }

    @Override
    public String helpString() {
        return "Index a document";
    }

    @Override
    public void run(String argumentsLine) throws Exception {
        Scanner scanner = new Scanner(System.in);

        System.out.println("document key: ");
        String key = scanner.nextLine();
        System.out.println("file path: ");
        String filePath = scanner.nextLine();
        String fileContent = getFileContent(filePath);
        Document document = new Document(fileContent);
        System.out.println("Processing...");
        try {
            webClientFactory.getClient("/documents/{key}")
                    .setPathVariables(Map.of("key", key))
                    .post(document, Void.class);
            System.out.println("Done");
        } catch (HttpClientErrorException.Conflict e) {
            System.err.println("Unable to index the document: duplicate key");
        }
    }

    private String getFileContent(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        File file = path.toFile();
        if (!file.exists() || !file.isFile()) {
            throw new RuntimeException("File not found");
        }
        return Files.readString(path);
    }

}

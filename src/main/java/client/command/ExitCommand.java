package client.command;

public class ExitCommand implements ICommand {
    @Override
    public String getCommand() {
        return "exit";
    }

    @Override
    public String helpString() {
        return "exit";
    }

    @Override
    public void run(String argumentsLine) {
        System.out.println("Have a Nice Day :)");
        System.exit(0);
    }
}

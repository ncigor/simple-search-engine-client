package client.command.getdocument;

import client.command.ICommand;
import client.command.bean.Document;
import client.web.WebClientFactory;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Map;

public class ReadDocumentCommand implements ICommand {
    private final WebClientFactory webClientFactory;

    public ReadDocumentCommand(WebClientFactory webClientFactory) {
        this.webClientFactory = webClientFactory;
    }

    @Override
    public String getCommand() {
        return "get";
    }

    @Override
    public String helpString() {
        return "get document by key. Format `get <document key>`";
    }

    @Override
    public void run(String key) {
        System.out.println("Processing...");
        try {
            Document document = webClientFactory.getClient("/documents/{key}")
                    .setPathVariables(Map.of("key", key.trim()))
                    .get(Document.class);
            System.out.println(document.getText());
        } catch (HttpClientErrorException.NotFound e) {
            System.out.println("Document not found by key: " + key);
        }
    }
}

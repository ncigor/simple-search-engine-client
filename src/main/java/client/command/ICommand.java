package client.command;

public interface ICommand {
    String getCommand();

    String helpString();

    void run(String argumentsLine) throws Exception;
}

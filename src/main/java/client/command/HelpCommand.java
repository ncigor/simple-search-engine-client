package client.command;

import java.util.List;

public class HelpCommand implements ICommand {
    private final List<ICommand> commands;

    public HelpCommand(List<ICommand> commands) {
        this.commands = commands;
    }

    @Override
    public String getCommand() {
        return "help";
    }

    @Override
    public String helpString() {
        return "print this help";
    }

    static String help = "";

    @Override
    public void run(String argumentsLine) throws Exception {
        for (ICommand command : commands) {
            System.out.println(command.getCommand() + "\t - \t" + command.helpString());
        }
        System.out.println();
    }
}

package client;

import client.command.ExitCommand;
import client.command.HelpCommand;
import client.command.ICommand;
import client.command.IncorrectCommandHandler;
import client.command.getdocument.ReadDocumentCommand;
import client.command.index.IndexDocumentCommand;
import client.command.runner.CommandManager;
import client.command.search.SearchCommand;
import client.web.WebClientFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Application {
    private static final int BASE_URI_ARG_INDEX = 0;
    private static final String DEFAULT_SERVER_URI = "http://localhost:8080/";

    public static void main(String[] args) throws Exception {
        String baseUri = obtainBaseUri(args);
        System.out.println("CLI for search server");
        System.out.println("Server URI: " + baseUri);
        System.out.println("Type `help' to find out more about the interface");

        List<ICommand> commands = configCommands(baseUri);

        new CommandManager(commands, new IncorrectCommandHandler()).run();
    }
    private static List<ICommand> configCommands(String baseUri) {
        WebClientFactory webClientFactory = new WebClientFactory(baseUri);
        List<ICommand> commands = new ArrayList<>();
        HelpCommand helpCommand = new HelpCommand(commands);
        commands.add(helpCommand);
        commands.add(new ExitCommand());
        commands.add(new ReadDocumentCommand(webClientFactory));
        commands.add(new IndexDocumentCommand(webClientFactory));
        commands.add(new SearchCommand(webClientFactory));
        return commands;
    }

    private static String obtainBaseUri(String[] args) {
        return Optional.ofNullable(args)
                .filter(arguments -> arguments.length > BASE_URI_ARG_INDEX)
                .map(arguments -> arguments[BASE_URI_ARG_INDEX])
                .map(String::trim)
                .filter(baseUri -> !baseUri.isBlank())
                .orElse(DEFAULT_SERVER_URI);
    }
}

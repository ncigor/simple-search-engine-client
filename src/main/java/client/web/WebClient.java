package client.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class WebClient {
    private String baseUri;
    private Map<String, String> pathVariables = Collections.emptyMap();
    private Map<String, String> queryVariables;

    public WebClient(String baseUri, String... uri) {
        this.baseUri = baseUri + String.join("", uri);
    }

    public WebClient setPathVariables(Map<String, String> pathVariables) {
        this.pathVariables = pathVariables;
        return this;
    }

    public WebClient setQueryVariables(Map<String, String> queryVariables) {
        this.queryVariables = queryVariables;
        return this;
    }

    private String uriString() throws URISyntaxException {
        Map<String, String> encodedParams = pathVariables.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e ->
                        URLEncoder.encode(e.getValue(), StandardCharsets.UTF_8)
                ));
        URI uri = new UriTemplate(baseUri).expand(encodedParams);

        URIBuilder uriBuilder = new URIBuilder(uri);
        if (null != queryVariables) {
            queryVariables.forEach(uriBuilder::addParameter);
        }
        return uriBuilder.build().toString();
    }

    public <T> T post(Object body, Class<T> responseType) {
        return execute(body, HttpMethod.POST, responseType);
    }

    public <T> T get(Class<T> responseType) {
        return execute(null, HttpMethod.GET, responseType);
    }

    public <T> T execute(Object body, HttpMethod httpMethod, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(getBody(body), headers);

        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(
                    uriString(), httpMethod, entity, responseType);
            return responseEntity.getBody();
        } catch (HttpClientErrorException e) {
            System.err.println("HTTP error on Server Request: " + e.getStatusCode() + " " + e.getMessage());
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getBody(Object body) {
        String bodyString = null;
        if (body != null) {
            try {
                bodyString = new ObjectMapper().writeValueAsString(body);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        return bodyString;
    }

}

package client.web;

public class WebClientFactory {

    private final String baseUri;

    public WebClientFactory(String baseUri) {
        this.baseUri = baseUri;
    }

    public WebClient getClient(String... uri) {
        return new WebClient(baseUri, uri);
    }
}

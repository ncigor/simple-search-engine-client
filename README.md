## Console Client for Simple Search Server
This is a console client for simple search server

## Installation
To compile source code into a jar file, please run script  
    `sh gradlew build` on Mac OS  
    `gradlew build` on Windows OS  
This instruction collect all required libs and package all into a file by location `build/libs/client.jar`
    
## Run App
If you have compiled file `build/libs/client.jar` you can run it with command  
    `java -jar build/libs/client.jar`   

It starts an app with default server address : `http://localhost:8080`   
If you would like to setup another server address you can do it with command:  
    `java -jar build/libs/client.jar <server-uri>`
  
## Additional Links
[Simple Search Engine Server](
https://gitlab.com/ncigor/simple-search-engine)